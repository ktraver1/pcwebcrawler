"""
This file is lambda handler that adds row to recipes table and returns the recipe_id that it created 
@author Kevin Travers
"""
import boto3
import logging
import pymysql
logger = logging.getLogger()
logger.setLevel(logging.INFO)
def lambda_handler(event, context):
    """
    This method starts step function execution
    @param url String
    @param recipe id Int
    """
    recipe_hash = create_recipe_id(event)
    return recipe_hash

def create_recipe_id(recipe_hash):
    """
    This method creates recipe row in database and gets recipe id used to assocaite with recipe
    @param recipe_name String name of the recipe
    """
    try:
        print("connecting to client")
        #rds settings get values stored encrypted in aws parameter store
        ssm = boto3.client('ssm')
        #passowrd to connect to database
        db_password = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Password', WithDecryption=True)['Parameter']['Value']
        #username to connect to database
        db_username = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Username', WithDecryption=True)['Parameter']['Value']
        # aws endpoint is the location of the database
        db_endpoint = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Endpoint', WithDecryption=True)['Parameter']['Value']
        # name of the database
        db_name     = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Name', WithDecryption=True)['Parameter']['Value']
        # extract recipe name
        recipe_name = recipe_hash["name"]
        # establish connection to rds database
        try:
            conn = pymysql.connect(db_endpoint, user=db_username, passwd=db_password, db=db_name, connect_timeout=5)
        except Exception as e:
            logger.error("ERROR: {}".format(e))
            print("ERROR: {}".format(e))
            raise e
        logger.info("SUCCESS: Connection to RDS MySQL instance succeeded")
        #create recipe id
        result = None
        with conn.cursor() as cur:
            sql     = 'insert into recipes (name) values(%s)'
            values  = (recipe_name)
            cur.execute(sql, values)
            conn.commit()
            result = cur.lastrowid
        if not result:
            # something went wrong send to failed queue
            logger.info("FAILURE: inserting to recipes failed")
            raise "FAILURE: inserting to recipes failed"
        # everything worked move on to next step and pass recipe id
        recipe_hash["recipe_id"] = result
        return recipe_hash 
    except Exception as e:
        logger.debug("ERROR: {}".format(e))
        raise e
if __name__=="__main__":
    result = create_recipe_id({"name":"Roasted Salmon"})
    print(result)