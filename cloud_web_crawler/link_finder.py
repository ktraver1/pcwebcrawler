import bs4 as bs
import requests
import urllib.request
from domain import *
import time


def find_links(url,homepage):
    """
    Get all links on url html page, returns all valid links
    @param string url (name.example.com/path/to/page)
    @param string homepage (name.example.com)
    @return set (String of valid urls)
    """
    links = set()
    try:
        # empty or null input is not valid
        if(url or homepage):
            # this will help hide the fact that program is not a bot
            # missing User Agent header will cause some sites to detect that request are from a bot
            # this is becuase many sites will block request not using a browser
            headers = requests.utils.default_headers()
            headers.update({
                'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
            })
            # backoff that’s proportional to how long the site took to respond to my request
            response_start = time.time()
            source = requests.get(url, headers=headers)
            #handle utf-8 was cauisng some links to fail
            soup = bs.BeautifulSoup(source.content.decode('utf-8','ignore'),"lxml")
            response_delay = time.time() - response_start
            #loop urls found on page 
            for url in soup.find_all('a'):
                #check if links are valid and if so add it set
                link = url.get('href')
                link = join_URL(homepage,link)
                if(is_valid_link(link,homepage)):
                    links.add(link)
            # so doesn't request too much from there server too fast so wait
            # wait 10x longer than it took them to respond
            # slows down how fast can crawl but will help prevent being blocked
            time.sleep(10*response_delay)  


        else:
            raise ValueError("Input can't be empty or None")
    except Exception as e:
        raise e
    #return links
    return links

def is_valid_link(url,homepage):
    """
    Only urls that have same domain as targer homepage will be valid
    dont want to scrap the enitre internet
    Check if inputs are empty/none and check if have same domain
    @param string url (name.example.com/path/to/page)
    @param string homepage (name.example.com)
    @return boolean if valid link
    """
    result = False
    try:
        #empty or null input is not valid
        if(url and homepage):
            #check if url is valid by checking if both have same domain
            url_domain = get_domain_name(url)
            homepage_domain = get_domain_name(homepage)
            if(url_domain == homepage_domain):
                result = True
    except Exception as e:
        raise e
    return result

if __name__ == "__main__":
    url = 'https://pythonprogramming.net/'
    x = find_links("https://pythonprogramming.net/",url)
    print(x)