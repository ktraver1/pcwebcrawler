import threading
import multiprocessing
from spider import Spider
from domain import *
from cloud_library import *


PROJECT_NAME = 'PCWebCrawler'
HOMEPAGE = 'https://pythonprogramming.net/'
DOMAIN_NAME = get_domain_name(HOMEPAGE)
NUMBER_OF_THREADS = multiprocessing.cpu_count()#number of cores of computers
#Spider(PROJECT_NAME, HOMEPAGE, DOMAIN_NAME)
# Create worker threads (will die when main exits)
def create_workers():
    for _ in range(NUMBER_OF_THREADS):
        t = threading.Thread(target=work)
        #t.daemon = True
        t.start()

# Do the next job in the queue
def work():
    aws = AwsAPI()
    queue_name = PROJECT_NAME+"Queue"
    url = aws.pull_queue(queue_name)
    while url:
        Spider.crawl_page(url)
        url = aws.pull_queue(queue_name)

#create_workers()
aws = AwsAPI()
queue_name = PROJECT_NAME+"Queue"
table_name = PROJECT_NAME+"Table"
#aws.add_item_to_dynamodb_table(table_name,"https://pythonprogramming.net/")
#aws.push_to_queue(queue_name,"https://pythonprogramming.net/")
#Spider.crawl_page("https://pythonprogramming.net/")
import boto3

# Create an SNS client
client = boto3.client('sns')

# Publish a simple message to the specified SNS topic
response = client.publish(
    TopicArn='arn:aws:sns:us-east-1:138103392229:RecipeGenerator',
    Message='string',
)