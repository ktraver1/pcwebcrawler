import boto3
import datetime
import json
import re
def lambda_handler(event, context):
        try:
                region = "us-east-1"
                recipe_id = event['Records'][0]['Sns']['Message']
                #TODO better way to get arm for step_function not hard coded in
                # The Amazon Resource Name (ARN) of the state machine to execute.
                # Example - arn:aws:states:us-west-2:112233445566:stateMachine:HelloWorld-StateMachine
                STATE_MACHINE_ARN = 'arn:aws:states:us-east-1:138103392229:stateMachine:delete_recipe_step_function'

                #The name of the execution
                #maybe use url as extra fail safe to amke sure no duplicates



                EXECUTION_NAME = str(recipe_id)
                
                 

                #The string that contains the JSON input data for the execution
                INPUT_DICT = {"recipe_id": recipe_id}
                INPUT = json.dumps(INPUT_DICT)

                sfn = boto3.client('stepfunctions')

                response = sfn.start_execution(
                stateMachineArn=STATE_MACHINE_ARN,
                name=EXECUTION_NAME,
                input=INPUT
                )

                #display the arn that identifies the execution
                print(response)
                 
        except Exception as e:
            raise e
        return {"Status":"Success"}

