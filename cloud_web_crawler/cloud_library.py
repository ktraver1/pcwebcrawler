import boto3
import botocore
import json
import decimal
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

# Helper class to convert a DynamoDB item to JSON.
#got this from aws documentation
import decimal
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

class AwsAPI:

    def __init__(self):
        pass
    #terraform will create table
    def create_dynamodb_table(self,table_name,primary_key="url",region="us-east-1",read_capacity=5,write_capacity=5):
        """
        method used to create dynamodb table in aws
        @table_name String name of the table
        @primary_key String name for the primary key for table (optinal default value:"url") needs to be unique value in table
        @region String region table exist in aws(optinal default value:us-east-1")
        @read_capacity read capactiy (optinal default value:5)
        @write_capacity write capacity (optinal default value:5)
        """
        try:
            dynamodb = boto3.client('dynamodb', region_name=region)
            table = dynamodb.create_table(
            TableName=table_name,
            KeySchema=[
                {
                    'AttributeName': primary_key,
                    'KeyType': 'HASH'  #Partition key
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': primary_key,
                    'AttributeType': 'S'
                }

            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': read_capacity,
                'WriteCapacityUnits': write_capacity
            }
            )
        #if resource already created raise error
        except dynamodb.exceptions.ResourceInUseException as e:
            raise Exception('ResourceAlreadyExist')
        except Exception as e:
            raise e
    def delete_dynamodb_table(self,table_name,region="us-east-1"):
        """
        method used to create dynamodb table in aws
        @table_name String name of the table
        @region String region table exist in aws(optinal default value:us-east-1")
        """
        try:
            #check if resouces is exist
            dynamodb_client = boto3.client('dynamodb', region_name=region)

            existing_tables = dynamodb_client.list_tables()['TableNames']
            if table_name in existing_tables:
                dynamodb = boto3.resource('dynamodb', region_name=region)

                table = dynamodb.Table(table_name)

                table.delete()
        except dynamodb.ResourceNotFoundException as e:
            raise Exception("Table does not exist")
        except Exception as e:
            raise e
    def get_dynamodb_table(self,table_name,region='us-east-1'):
        """
        method used to get dynomadb table
        @param table_name String name of the table
        @param region String region table exist in aws(optinal default value:us-east-1")
        @return table boto3.resource
        """
        try:
            dynamodb = boto3.resource('dynamodb', region_name=region)
            table = dynamodb.Table(table_name)
            return table
        except dynamodb.ResourceNotFoundException as e:
            raise Exception("Table does not exist")
        except Exception as e:
            raise Exception



    def add_item_to_dynamodb_table(self,table_name,url,crawled=0,region='us-east-1',key="url",item="crawled"):
        """
        method used to add item dynomadb table
        @param table_name String name of the table
        @param url String new link that will be crawled
        @param crawled int 1 alrady crawled 0 have not been crawled, default 0 not crawled
        @param region String region table exist in aws(optinal default value:us-east-1")
        """
        try:
            #add item to dynomdb
            dynamodb = boto3.resource('dynamodb', region_name=region)

            table = dynamodb.Table(table_name)

            response = table.put_item(
            Item={
                    key: url,
                    item: crawled,
                }
            )
            return response
        except ClientError as e:
            print(e.response['Error']['Message'])
            raise Exception("Failed to add table")
        except dynamodb.ResourceNotFoundException as e:
            raise Exception("Table does not exist")
        except Exception as e:
            raise e
        
    def remove_item_dynamodb_table(self,table_name,url,region="us-east-1",key="url"):
        """
        method used to add item dynomadb table
        @param table_name String name of the table
        @param url String new link that will be crawled
        @param key String partion key for the table
        @param region String region table exist in aws(optinal default value:us-east-1")
        """
        try:
            dynamodb = boto3.resource('dynamodb', region_name=region)

            table = dynamodb.Table(table_name)
            response = table.delete_item(
                Key={
                    key: url
                }
            )
        except ClientError as e:
            if e.response['Error']['Code'] == "ConditionalCheckFailedException":
                print(e.response['Error']['Message'])
            else:
                raise
        except dynamodb.ResourceNotFoundException as e:
            raise Exception("Table does not exist")
        else:
            #add loging here
            pass
            #print("DeleteItem succeeded:")
            #print(json.dumps(response, indent=4, cls=DecimalEncoder))

    def update_item_dynamodb_table(self,table_name,url,crawled=1,region='us-east-1',key="url",update_field="crawled"):
        """
        method used to update item dynomadb table
        @param table_name String name of the table
        @param url String new link that will be crawled
        @param crawled int 1 alrady crawled 0 have not been crawled, default 1 setting it to cwarled
        @param region String region table exist in aws(optinal default value:us-east-1")
        @param key String partion key for the table
        """
        try:
            dynamodb = boto3.resource('dynamodb', region_name=region)
            table = dynamodb.Table(table_name)
            response = table.update_item(
                Key={
                    key: url
                },
                UpdateExpression="set "+update_field+" = :c",
                ExpressionAttributeValues={
                    ':c': crawled
                },
                ReturnValues="UPDATED_NEW"
            )
            return response
        except ClientError as e:
            print(e.response['Error']['Message'])
            raise Exception("Failed to update table")

        except dynamodb.ResourceNotFoundException as e:
            raise Exception("Table does not exist")
    def get_item_dynamodb_table(self,table_name,url,region='us-east-1',key="url",item="crawled"):
        """
        method used to get item from dynomadb table
        @param table_name String name of the table
        @param url String new link that will be crawled
        @param crawled int 1 alrady crawled 0 have not been crawled, default 1 setting it to cwarled
        @param region String region table exist in aws(optinal default value:us-east-1")
        @param key json partion key for the table
        """
        try:
            dynamodb = boto3.resource('dynamodb', region_name=region)
            table = dynamodb.Table(table_name)
            response = table.get_item(
                Key={
                    key: url
                }
            )
        except ClientError as e:
            print(e.response['Error']['Message'])
            raise Exception("Failed to get item in table")

        except dynamodb.ResourceNotFoundException as e:
            raise Exception("Table does not exist")
        else:
            item = response['Item']
            return item
    def is_in_dynamodb_table(self,table_name,url,region='us-east-1',key="url",item="crawled"):
        """
        method used to check if item is in dynomadb table
        @param table_name String name of the table
        @param url String new link that will be crawled
        @param crawled int 1 alrady crawled 0 have not been crawled, default 1 setting it to cwarled
        @param region String region table exist in aws(optinal default value:us-east-1")
        @param key String partion key for the table
        """
        result = False
        try:
            #call get_item to see if it is in table, if fails and throw erro then item not in table
            item = self.get_item_dynamodb_table(table_name,url,region,key,item)
            result = True
        except Exception as e:
            pass
        return result

    #will use terraform to create sqs
    def create_queue(self,queue_name):
        """
        method used to create sqs
        @param queue_name String name of the sqs to be created
        """
        try:
            # Get the service resource
            sqs = boto3.resource('sqs')
            # Create the queue. This returns an SQS.Queue instance
            queue = sqs.create_queue(QueueName=queue_name, Attributes={'DelaySeconds': '5'})
        except Exception as e:
            raise e

    def delete_queue(self,queue_name):
        """
        method used to delete sqs
        @param queue_name String name of the sqs to be deleted
        """
        try:
            # Get the service resource
            client = boto3.client('sqs')
            queue = client.create_queue(QueueName=queue_name)
            # Delete the queue.
            client.delete_queue(QueueUrl=queue['QueueUrl'])
        except Exception as e:
            raise e

    def get_queue(self,queue_name):
        """
        method used get sqs
        @param queue_name String name of the sqs
        """
        try:
            # Get the service resource
            sqs = boto3.resource('sqs')

            # Get the queue. This returns an SQS.Queue instance
            queue = sqs.get_queue_by_name(QueueName=queue_name)

            # You can now access identifiers and attributes
            return queue
        except Exception as e:
            raise e
    def peek_queue(self,queue_name):
        """
        method used peek next item from sqs
        @param queue_name String name of the sqs
        @return item String name of the item pushed to sqs
        """
        try:
            # Create SQS resource
            sqs = boto3.resource('sqs')
            # Get the queue
            queue = sqs.get_queue_by_name(QueueName=queue_name)
            # Get ApproximateNumberOfMessages in sqs
            return queue.attributes["ApproximateNumberOfMessages"]
        except Exception as e:
            raise e
    def push_to_queue(self,queue_name,item):
        """
        method used push item to sqs
        @param queue_name String name of the sqs
        @param item String name of the item pushed to sqs
        """
        try:
            # Get the service resource
            sqs = boto3.resource('sqs')

            # Get the queue
            queue = sqs.get_queue_by_name(QueueName=queue_name)

            # Create a new message
            response = queue.send_message(MessageBody='boto3', MessageAttributes={
                'URL': {
                    'StringValue': item,
                    'DataType': 'String'
                }
            })
        except Exception as e:
            raise e
    def pull_queue(self,queue_name):
        """
        method used pull item to sqs
        @param queue_name String name of the sqs
        @return json message in sqs
        """
        try:
            # Create SQS resource
            sqs = boto3.resource('sqs')
            # Get the queue
            queue = sqs.get_queue_by_name(QueueName=queue_name)
             # Create SQS client
            sqs = boto3.client('sqs')           
            # Get the queue url
            queue_url = queue.url
            # Receive message from SQS queue
            response = sqs.receive_message(
                QueueUrl=queue_url,
                AttributeNames=[
                    'SentTimestamp'
                ],
                MaxNumberOfMessages=1,
                MessageAttributeNames=[
                    'All'
                ],
                VisibilityTimeout=0,
                WaitTimeSeconds=0
            )
            #if no messages then nothing pulled and return none since nothing pulled
            result = None
            if 'Messages' in response:
                message = response['Messages'][0]
                receipt_handle = message['ReceiptHandle']
                # Let the queue know that the message is processed
                # Delete received message from queue
                sqs.delete_message(
                    QueueUrl=queue_url,
                    ReceiptHandle=receipt_handle
                )
                result = message["MessageAttributes"]["URL"]["StringValue"]
            return result
            
        except Exception as e:
            raise e

if __name__=="__main__":
    aws = AwsAPI()
    #aws.create_dynamodb_table("test")
    page = "google.comie"
    #aws.add_item_to_dynamodb_table("test",page)
    response = aws.is_in_dynamodb_table("test",page)
    print(response)
