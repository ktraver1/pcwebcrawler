import boto3

def lambda_handler(event,context):
    """
    method used push item to sqs
    @param queue_name String name of the sqs
    @param item String name of the item pushed to sqs
    """
    try:
        # Get the service resource
        sqs = boto3.resource('sqs')
        recipe_url = event["recipe_url"]

        # Get the queue
        queue = sqs.get_queue_by_name(QueueName="PCAddRecipeRetryQueue")

        # Create a new message
        response = queue.send_message(MessageBody='boto3', MessageAttributes={
            'URL': {
                'StringValue': recipe_url,
                'DataType': 'String'
            }
        })
    except Exception as e:
        raise e

