import boto3
import botocore
import json
import decimal
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event,context):
    """
    Instructions are stroed in dynamodb table, the key being the recipe_id and each feild is diffrenrt instrunction
    @param even is recipe dict with all metadata of recipe
    """
    try:
        #rds settings get values stored encrypted in aws parameter store
        #ssm = boto3.client('ssm')
        # table name of dynaomdb for instrucntions
        table_name = "PCRecipeReviewsTable" #ssm.get_parameter(Name='/PersonalChef/Prod/DynamoDB/Instructions', WithDecryption=True)['Parameter']['Value']
        recipe_id = event["recipe_id"]
        reviews = event["reviews"]
        # check reviews is empty, if empty nothing to put into table so can skip this step
        if reviews is not None:
            item_dict = create_item_hash(recipe_id,reviews)
            add_item_to_dynamodb_table(item_dict,table_name)    
        return event
    except Exception as e:
        # start deleting recipe step functions
        logger.error("ERROR: {}".format(e))
        raise e  
def create_item_hash(recipe_id, reviews_list):
    """
    This method creates item to be added to dynamodb table
    @param recipe_id int unique recipe identifier
    @param reviews_list list
    """
    item_dict = {"RecipeID":recipe_id}
    for index,review in enumerate(reviews_list):
        item_dict[str(index)] = review
    return item_dict

    


def add_item_to_dynamodb_table(recipe_item,table_name,region='us-east-1'):
    """
    method used to add item dynomadb table
    @param recipe_item array instructions steps for recipe
    @param table_name String name of dynamo table 
    @param region String region table exist in aws(optinal default value:us-east-1")
    """
    try:
        # add item to dynomdb
        dynamodb = boto3.resource('dynamodb', region_name=region)
        # connect to table and put item into table
        table = dynamodb.Table(table_name)
        response = table.put_item(
            Item=recipe_item
        )
        return response
    except ClientError as e:
        logging.debug(e.response['Error']['Message'])
        raise Exception("Failed to add to table")
    except dynamodb.ResourceNotFoundException as e:
        logging.debug("Table does not exist")
        raise Exception("Table does not exist")
    except Exception as e:
        logging.debug(e)
        raise e

if __name__=="__main__":
    recipe_hash = { "recipe_id": 1,'name': 'Roasted Salmon with Shallot Grapefruit Sauce', 'ingredients': ['4 skinless salmon fillets, 5 to 6 ounces each', '1/4 teaspoon salt, plus more for seasoning', '2 ruby red grapefruits', '2 teaspoons olive oil', '1 tablespoon minced shallot', '1 teaspoon freshly grated ginger', '2 1/2 teaspoons honey', 'Pinch cayenne pepper', '2 teaspoons lemon juice', '2 tablespoons thinly sliced basil leaves'], 'instructions': [{'@type': 'HowToStep', 'text': 'Preheat the oven to 350 degrees F.'}, {'@type': 'HowToStep', 'text': 'Season the salmon with 1/4 teaspoon salt, place in a baking dish and roast until cooked through, about 18 minutes.'}, {'@type': 'HowToStep', 'text': 'While the salmon is cooking prepare the sauce. Cut 1 of the grapefruits into sections by cutting off the top and bottom of the fruit, then standing it on 1 end, cut down the skin to remove the pith and peel. Then, with a paring knife, remove each segment of fruit from its casing and cut the segments in half. Set the segment pieces aside. Juice the other grapefruit and set the juice aside.'}, {'@type': 'HowToStep', 'text': 'In a medium skillet, heat the oil over a medium heat. Add the shallot and saute until softened, about 2 minutes. Add the ginger, grapefruit juice, honey, and cayenne pepper and bring to simmer. Cook until sauce is reduced by about half about, 10 minutes. Add lemon juice and season with salt, to taste. Right before serving, toss the grapefruit pieces and basil into the sauce. Put the salmon onto a serving dish. Spoon sauce over the salmon and serve.'}], 'active_cooking_time': 1080.0, 'total_cooking_time': 1980.0, 'recipe_url': 'https://www.foodnetwork.com/recipes/ellie-krieger/roasted-salmon-with-shallot-grapefruit-sauce-recipe-1947996', 'calories': 345.0, 'fatContent': 18.0, 'carbohydrateContent': 16.0, 'proteinContent': 29.0, 'difficulty': 'easy', 'yield': '4 servings', 'servings_size': 4.0, 'categories': 'main-dish', 'description': 'Roasted Salmon with Shallot Grapefruit Sauce; Ellie Krieger', 'keywords': ['Healthy', 'American', 'Fish', 'Salmon', 'Main Dish', 'Diabetes-Friendly', 'Roasting', 'Gluten Free', 'Low Sodium'], 'reviews': [{'@type': 'Review', 'author': {'@type': 'Person', 'name': 'jvharmon2'}, 'reviewRating': {'@type': 'Rating', 'ratingValue': 5, 'worstRating': '1', 'bestRating': '5'}, 'reviewBody': 'Excellent! \nThe blend of flavors: tart grapefruit, sweet honey and ginger, and the smallest pinch of basil, is a melange of subtlety. This recipe offers a new and exciting twist on salmon. \n\nI altered the preparation of salmon. I brushed it with butter and seasoned it with a pinch of Himalayan salt before wrapping it in parchment. Cooked this way the salmon is juicy, and not dried out. ', 'datePublished': '2017-09-18'}, {'@type': 'Review', 'author': {'@type': 'Person', 'name': 'katie1802493700'}, 'reviewRating': {'@type': 'Rating', 'ratingValue': 5, 'worstRating': '1', 'bestRating': '5'}, 'reviewBody': 'This is so delicious! \xa0Was surprised how well the flavors went together! \xa0A keeper!', 'datePublished': '2015-10-06'}, {'@type': 'Review', 'author': {'@type': 'Person', 'name': 'TMiNi'}, 'reviewRating': {'@type': 'Rating', 'ratingValue': 5, 'worstRating': '1', 'bestRating': '5'}, 'reviewBody': "Absolutely delicious! Family still talks about this dish to this day. You don't need a lot of sauce because it's supposed to be a drizzle on top ... accent the tasty salmon without overpowering it. Saving this recipe!", 'datePublished': '2014-04-30'}, {'@type': 'Review', 'author': {'@type': 'Person', 'name': 'Catherine B.'}, 'reviewRating': {'@type': 'Rating', 'ratingValue': 5, 'worstRating': '1', 'bestRating': '5'}, 'reviewBody': 'Always looking for new ways to serve salmon. This was a real winner. The combination of shallots, grapefruit juice, fresh ginger, honey, cayenne, lemon juice, and basil in the sauce really worked for me. I loved it. ', 'datePublished': '2019-04-15'}, {'@type': 'Review', 'author': {'@type': 'Person', 'name': 'elizabeth navisky'}, 'reviewRating': {'@type': 'Rating', 'ratingValue': 2, 'worstRating': '1', 'bestRating': '5'}, 'reviewBody': 'Strange combo of flavors. Didn’t work for us. ', 'datePublished': '2019-01-31'}, {'@type': 'Review', 'author': {'@type': 'Person', 'name': 'Killo Hopkins'}, 'reviewRating': {'@type': 'Rating', 'ratingValue': 5, 'worstRating': '1', 'bestRating': '5'}, 'reviewBody': 'looks great', 'datePublished': '2018-10-23'}, {'@type': 'Review', 'author': {'@type': 'Person', 'name': 'EricaRoelofs'}, 'reviewRating': {'@type': 'Rating', 'ratingValue': 4, 'worstRating': '1', 'bestRating': '5'}, 'reviewBody': 'This was good, but we made it slightly different which I think enhanced the dish. I put it over a spinach salad with goat cheese and toasted walnuts. The goat cheese really pulled the dish together.', 'datePublished': '2018-10-03'}, {'@type': 'Review', 'author': {'@type': 'Person', 'name': 'please.do.it'}, 'reviewRating': {'@type': 'Rating', 'ratingValue': 4, 'worstRating': '1', 'bestRating': '5'}, 'reviewBody': 'super cool', 'datePublished': '2018-09-26'}, {'@type': 'Review', 'author': {'@type': 'Person', 'name': 'doloreswilson35'}, 'reviewBody': 'Hope to make this  next week.      Can I use a different fish such as cod..', 'datePublished': '2018-08-11'}, {'@type': 'Review', 'author': {'@type': 'Person', 'name': 'Anonymous'}, 'reviewBody': 'I’m excited to make this dish tonight. Any thoughts on what kind of wine conpliments this sauce/preparation?\xa0', 'datePublished': '2018-06-23'}], 'author': 'Ellie Krieger', 'rating': 4.5, 'review_count': 78, 'recipe_image_url': 'https://food.fnr.sndimg.com/content/dam/images/food/fullset/2011/6/6/0/EK0307_Roasted-Salmon-with-Shallot-Grapefruit-Sauce_s4x3.jpg.rend.hgtvcom.406.305.suffix/1382539975573.jpeg', 'saturatedFatContent': 3.5, 'cholesterolContent': 0.085, 'sugarContent': None, 'fiberContent': 1.0, 'sodiumContent': 0.23}
    lambda_handler(recipe_hash,"")