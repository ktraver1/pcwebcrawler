import bs4 as bs
import requests
import urllib.request
import json
import logging
logging.basicConfig(level=logging.DEBUG)

class Recipe():
    """
    This class will create object for a recipe and will have all details needed to create recipe
    """
    def __init__(self,recipe_url):
        """
        @param recipe_url of the recipe
        """
        self.recipe_setup(recipe_url)
    def recipe_setup(self,recipe_url):
        """
        setup the recipe
        use BeautifulSoup to navigate webpage and set json of all data needed for recipe  
        """
        self.set_url(recipe_url)
        self.set_recipe_json()
    def set_url(self,recipe_url):
        """
        This Method sets url used by class to exctract data, called when initilzed and can me used again to change the url
        @param recipe_url String url page 
        """
        try:
            # empty or null input is not valid
            if recipe_url:
                # this will help hide the fact that program is not a bot
                # missing User Agent header will cause some sites to detect that request are from a bot
                # this is becuase many sites will block request not using a browser
                headers = requests.utils.default_headers()
                headers.update({
                    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
                })
                source = requests.get(recipe_url, headers=headers)
                #handle utf-8 was cauisng some links to fail
                self.soup = bs.BeautifulSoup(source.content.decode('utf-8','ignore'),"lxml")
            else:
                raise ValueError("Input can't be empty or None")
        except Exception as e:
            raise e
    def set_recipe_json(self):
        """
            Method gets data about recipe from https://www.foodnetwork.com/
            @return json of data about recipe
        """
        try:
            result = None
            # Finds recipe data in url and sets recipe metadata to json
            # recipe metadata is in script
            for script in self.soup.find_all('script'):
                script_type = script.get('type')
                # Contains scripty type of application/ld+json
                if script_type == "application/ld+json":
                    recipe_arr=json.loads(str(script.contents[0]))
                    #loop through recipes json array to find recipe
                    for recipe_json in recipe_arr:
                        #recipe is data in url has type recipe
                        if recipe_json["@type"] == 'Recipe':
                            result = recipe_json
            # if recipe not found then raise error
            if not result:
                raise ValueError("Not a valid recipe url, page does not contain a recipe")
            else:
                self.recipe_json = result
        except Exception as e:
            logging.debug("ERROR GETTING JSON")
            raise e

    def get_recipe_name(self):
        """
        Method retrurns name of the recipe
        @return String recipe name
        """
        try:
            return self.recipe_json["name"]
        except Exception as e:
            logging.debug("ERROR GETTING Name")
            raise e  
    def get_recipe_ingredients(self):
        """
        Method retrurns ingreidents for the recipe
        @return Array of all ingredients
        """
        try:
            return self.recipe_json["recipeIngredient"]
        except Exception as e:
            logging.debug("ERROR GETTING recipeIngredients")
            raise e  
    def get_recipe_instructions(self):
        """
        Method retrurns instructions for the recipe
        @return Array of all instructions
        """
        try:
            #for instructions items are crawled as a dicts in a list, only need text value
            #example:
            #   {'@type': 'HowToStep', 
            #   'text': 'Preheat the oven to 350 degrees F.'}
            instuctions = []
            for instuction in self.recipe_json["recipeInstructions"]:
                instuctions.append(instuction["text"])
            return instuctions
        except Exception as e:
            logging.debug("ERROR GETTING recipe instructions")
            raise e
    def get_recipe_description(self):
        """
        Method retrurns description for the recipe
        @return String description
        """
        try:
            return self.recipe_json["image"]["description"]
        except Exception as e:
            logging.debug("ERROR GETTING recipe description")
            #raise e
            #optional feild return None for database
            return None
    def get_recipe_image(self):
        """
        Method retrurns image for the recipe
        @return String image url
        """
        try:
            return self.recipe_json["image"]["url"]
        except Exception as e:
            logging.debug("ERROR GETTING recipe image")
             #raise e
            #optional feild return None for database 
            return None
    def get_recipe_author(self):
        """
        Method retrurns author for the recipe
        @return String author
        """
        try:
            return self.recipe_json["author"][0]["name"]
        except Exception as e:
            logging.debug("ERROR GETTING author")
            #raise e
            #optional feild return None for database
            return None
    def get_recipe_reviews(self):
        """
        Method retrurns reviews for the recipe
        @return String array of reviews
        """
        try:
            #for reviews items are saved as a dicts in a list
            #example:
            """
            {
            "@type": "Review",
            "author": {
                "@type": "Person",
                "name": "jvharmon2"
            },
            "reviewRating": {
                "@type": "Rating",
                "ratingValue": 5,
                "worstRating": "1",
                "bestRating": "5"
            },
            "reviewBody": "Excellent! \nThe blend of flavors: tart grapefruit, sweet honey and ginger, and the smallest pinch of basil, is a melange of subtlety. This recipe offers a new and exciting twist on salmon. \n\nI altered the preparation of salmon. I brushed it with butter and seasoned it with a pinch of Himalayan salt before wrapping it in parchment. Cooked this way the salmon is juicy, and not dried out. ",
            "datePublished": "2017-09-18"
            }
            """
            reviews = []
            for review_hash in self.recipe_json["review"]:
                review = {}
                # check if name exists if it does not use anonymous instead
                if "author" in review_hash:
                    review["name"] = review_hash["author"]["name"]
                else:
                    review["name"] = "anonymous"
                # check if user left a rating, rating is optional
                if "reviewRating" in review_hash:
                    review["rating"] = review_hash["reviewRating"]["ratingValue"]
                #check if review exists if not then skip
                if "reviewBody" in review_hash:
                    review["review_body"] = review_hash["reviewBody"]
                    # the feild name cannot be a number must be a string so convert index to int
                    # the key is the feild name and value will be review
                    reviews.append(review)
            return reviews
        except Exception as e:
            logging.debug("ERROR GETTING reviews")
            #raise e
            #optional feild return None for database
            return None
    def get_recipe_active_cooking_time(self):
        """
        Method retrurns active cooking time for the recipe
        @return int active cooking time in P0Y0M0DT0H0M0.000S converted to seconds
        """
        try:
            #convert time to seconds
            return self.convert_time_seconds(self.recipe_json["cookTime"])
        except Exception as e:
            logging.debug("ERROR GETTING active_cooking_time")
            raise e
    def get_recipe_total_cooking_time(self):
        """
        Method retrurns total cooking time for the recipe
        @return int total cooking time in P0Y0M0DT0H0M0.000S converted to seconds
        """
        try:
            #convert time to seconds
            return self.convert_time_seconds(self.recipe_json["totalTime"])
        except Exception as e:
            logging.debug("ERROR GETTING total_cooking_time")
            raise e
    def convert_time_seconds(self,time_string):
        """
        Method retrurns time in seconds 
        @param time_string  string as P0Y0M0DT0H0M0.000S format
        @return Int total  time in seconds 
        """
        result = 0
        # 60 seconds in minute, 60 minutes in hour, 24 hours in day, 30 days in month, 12 months in year
        # so i don't think any recipes will take years, something like beer or liqour take months
        # but will keep because i wanna see what can take years to make
        converter_lambda = lambda time,multiplier : float(time)*multiplier
        result += converter_lambda(time_string.split("P")[1].split("Y")[0],(60*60*24*30*12))
        result += converter_lambda(time_string.split("Y")[1].split("M")[0],(60*60*24*30))
        result += converter_lambda(time_string.split("M")[1].split("DT")[0],(60*60*24))
        result += converter_lambda(time_string.split("DT")[1].split("H")[0],(60*60))
        result += converter_lambda(time_string.split("H")[1].split("M")[0],(60))
        result += converter_lambda(time_string.split("M")[2].split("S")[0],(1))
        return result
    def get_recipe_rating(self):
        """
        Method retrurns rating recipe
        @return float of rating
        """
        try:
            return self.recipe_json["aggregateRating"]["ratingValue"]
        except Exception as e:
            logging.debug("ERROR GETTING rating")
            #raise e
            #optional feild return None for database
            return None
    def get_recipe_review_count(self):
        """
        Method retrurns rating recipe reviewCount so to weight against future reviews
        @return int of review count
        """
        try:
            return self.recipe_json["aggregateRating"]["reviewCount"]
        except Exception as e:
            logging.debug("ERROR GETTING review_count")
            #raise e
            #optional feild return None for database
            return None    
    def get_recipe_keywords(self):
        """
        Method retrurns list of keywords for this receip
        @return array of keywords
        """
        try:
            return self.recipe_json["keywords"].split(",")
        except Exception as e:
            logging.debug("ERROR GETTING keywords")
            #raise e
            #optional feild return None for database
            return None
    
    def get_recipe_servings_description(self): 
        """
        Method retrurns yeild description of the recipe
        @return String yeild of recipe
        """
        try:
            return self.recipe_json["recipeYield"]
        except Exception as e:
            logging.debug("ERROR GETTING yield(servings_description)")
            raise e
    def get_recipe_servings(self): 
        """
        Method retrurns servings of the recipe, used for database filters
        @return int yeild of recipe
        """
        try:
            #converts string to servings, extract first number in string this is number of servings
            serving_arr = [float(s) for s in self.recipe_json["recipeYield"].split() if s.replace(".", "", 1).isdigit()]
            # return first number in array since first valid nubmer will be servings
            return serving_arr[0]
        except Exception as e:
            logging.debug("ERROR GETTING yield")
            raise e
    def get_recipe_categories(self):
        """
        Method retrurns what category recipe belongs too
        @return String categoriy
        """
        try:
            return self.recipe_json["recipeCategory"]
        except Exception as e:
            logging.debug("ERROR GETTING categories")
            #raise e
            #optional feild return None for database
            return None

    def get_recipe_nutrition(self):
        """
        Method retrurns nutrition infomation for the recipe
        @return Dict nutrion information
            {"@type": "NutritionInformation",
            "calories": "334 calorie",
            "fatContent": "11 grams",
            "saturatedFatContent": "3 grams",
            "cholesterolContent": "296 milligrams",
            "sodiumContent": "719 milligrams",
            "carbohydrateContent": "25 grams",
            "fiberContent": "4 grams",
            "proteinContent": "37 grams",
            "sugarContent": "8 grams"}
        """
        try:
            return self.recipe_json["nutrition"]
        except Exception as e:
            logging.debug("ERROR GETTING nutrition")
            raise e
    def get_recipe_difficulty(self): 
        """
        Method retrurns difficulty level of the recipe
        @return String difficulty easy,medium or hard
        """
        try:
            result = None
            # diffiuclty is not in same place as other feilds in url
            # to extract difficulty need to correct script
            # loop through all scripts
            for script in self.soup.find_all('script'):
                script_type = script.get('type')
                # only search script if type is text/javascript
                if script_type == "text/javascript":
                    script_string = str(script)
                    try:
                        # split difficulty from script string
                        # if not found will throw error
                        _, difficulty = script_string.split('"Difficulty",')
                        # if difficulty found parse to only extract difficulrty string 
                        result = difficulty.split('\"')[1]
                    except ValueError:
                        # this will search over scripts that don't have diffucly value and this will skip over them
                        # TODO: find a better way to solve this
                        #logging.debug("ERROR no difficulty")
                        pass
            # if difficulty not found raise error
            if not result:
                logging.debug("Difficulty not found")
                raise ValueError("Difficulty not found")
            else:
                return result
        except Exception as e:
            logging.debug("ERROR GETTING difficulty")
            raise e
    def convert_nutrition_to_grams(self,nutrition):
        """
        Method retrurns nutrional information string as float in grams
        @return String nutrition 500 miligrams becomes .05 
        """
        #milligram devide by 1000, gram nothing, killogram multiply by 1000
        #first check if valid param
        try:
            if nutrition == None:
                #if nutrition not inlcuded will be None
                return nutrition
            nutrition_arr = nutrition.split(" ")
            # nutrional value is first in array and units of mesaurement is second value
            value,unit = (nutrition_arr)
            # hash used to convert string to number in metric
            unit_converter={"kilograms":1000,"grams":1,"milligrams":.001,"kg":1000,"g":1,"mg":.001}
            return float(value)*unit_converter[unit]
        except Exception as e:
            logging.debug("ERROR converting to grams")
            raise e
if __name__=="__main__":
    recipe = Recipe("https://www.foodnetwork.com/recipes/food-network-kitchen/instant-pot-mashed-potatoes-5296006")
    name = recipe.get_recipe_difficulty()
    print(name)