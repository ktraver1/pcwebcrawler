import unittest
from domain import *

class TestDomain(unittest.TestCase):
    """
    Unit tests for domain.py
    """
    def test_get_sub_domain_name(self):
        #test get_sub_domain_name is working as intended with correct input
        sub_domain = get_sub_domain_name("http://test.kevintravers.net/")
        self.assertEqual(sub_domain, "test.kevintravers.net")

    def test_get_domain_name(self):
        #test get_domain_name is working as intended with correct input
        domain = get_domain_name("http://kevintravers.net/")
        self.assertEqual(domain, "kevintravers.net")

    def test_get_site_name(self):
        #test get_site_name is working as intended with correct input
        site_name = get_site_name("http://blog.kevintravers.net/")
        self.assertEqual(site_name, "kevintravers")

    def test_join_URL(self):
        #test join_URL is working as intended with correct inputs
        url = join_URL("http://kevintravers.net","path/to/home/page")
        self.assertEqual(url, "http://kevintravers.net/path/to/home/page")

    def test_get_sub_domain_name_error(self):
        #test get_sub_domain_name with incorrect input, should rasie error
        with self.assertRaises(Exception):
            sub_domain = get_sub_domain_name(1)

    def test_get_domain_name_error(self):
        #test get_domain_name with incorrect input, should rasie error
        with self.assertRaises(Exception):
            domain = get_domain_name("FOOBAR")

    def test_get_site_name_error(self):
        #test get_site_name with incorrect input, should rasie error
        with self.assertRaises(Exception):
            site_name = get_site_name("FOOBAR")

    def test_join_URL_error(self):
        #test join_URL with incorrect input, should rasie error
        with self.assertRaises(TypeError):
            url = join_URL("http://kevintravers.net",1)


if __name__ == "__main__":
    unittest.main()