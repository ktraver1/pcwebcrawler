"""
This file is lambda handler that deletes row of recipes instruction in dynaomdb
@author Kevin Travers
"""
import boto3
import logging
from botocore.exceptions import ClientError
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event,conext):
    recipe_id = event["recipe_id"]
    delete_item_from_dynamodb_table(recipe_id)
    return event


def delete_item_from_dynamodb_table(recipe_id,region='us-east-1'):
    """
    remove instructions for recipe using recipe id
    @param recipe_id int primary key 
    @param region String region table exist in aws(optinal default value:us-east-1")
    """
    try:
        # table name of dynaomdb for instrucntions
        table_name = "PCRecipeInstructionsTable" 
        dynamodb = boto3.resource('dynamodb', region_name=region)
        # connect to table and delete item from table
        table = dynamodb.Table(table_name)
        response = table.delete_item(
            Key={
                "RecipeID":recipe_id
            }
        )
        return response
    except ClientError as e:
        logging.debug(e.response['Error']['Message'])
        raise Exception("Failed to add to table")
    except dynamodb.ResourceNotFoundException as e:
        logging.debug("Table does not exist")
        raise Exception("Table does not exist")
    except Exception as e:
        logging.debug(e)
        raise e

