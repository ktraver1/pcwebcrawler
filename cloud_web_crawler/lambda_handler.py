from spider import Spider
from domain import *
from cloud_library import *
from link_finder import *
import os
import boto3

def lambda_handler(event, context):
    #Make these envrionmental varibles
    #get the project name and base homepage of the site that will be crawled
    PROJECT_NAME = os.environ['PROJECT_NAME']
    HOMEPAGE = os.environ['HOMEPAGE']
    DOMAIN_NAME = get_domain_name(HOMEPAGE)
    Spider(PROJECT_NAME, HOMEPAGE, DOMAIN_NAME)
    records=event["Records"]
    for record in records:
        url = record["messageAttributes"]["URL"]["stringValue"]
        Spider.crawl_page(url)
        publish_to_sns(url)
    return {"Status":"Success"}

def publish_to_sns(url):
    # Create an SNS client
    client = boto3.client('sns')

    # Publish a simple message to the specified SNS topic
    response = client.publish(
        TopicArn='arn:aws:sns:us-east-1:138103392229:RecipeGenerator',
        Message=url,
    )