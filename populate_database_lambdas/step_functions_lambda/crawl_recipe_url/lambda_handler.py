import boto3
from recipe import *

def lambda_handler(event, context):
    recipe_url = event["recipe_url"]
    recipe_hash = validate_recipe(recipe_url)
    return recipe_hash

def validate_recipe(recipe_url):
    """
    THis method validates the recipe from the url. as it creates the hash for recipe if any values are missing or incorrect
    It will fail and won't use this page to add recipes to backend
    Will fail if page doens' contain recipe or if missing/incoreect data on valid recipe page
    @param  recipe Url
    @return hash of recipe 
    """
    try:
        # create recipe object
        recipe = Recipe(recipe_url)
        # validate recipe object has all required values and return hash if valid
        recipe_hash = {}
        #requried feilds don't create recipe if not included
        recipe_hash["name"]                 = recipe.get_recipe_name()
        recipe_hash["ingredients"]          = recipe.get_recipe_ingredients()
        recipe_hash["instructions"]         = recipe.get_recipe_instructions()
        recipe_hash["active_cooking_time"]  = recipe.get_recipe_active_cooking_time()
        recipe_hash["total_cooking_time"]   = recipe.get_recipe_total_cooking_time()
        recipe_hash["recipe_url"]           = recipe_url
        #calories need to parse calorie from string to convert it to int
        recipe_hash["calories"]             = float(recipe.get_recipe_nutrition()["calories"].replace("calorie",""))
        recipe_hash["fatContent"]           = recipe.convert_nutrition_to_grams(recipe.get_recipe_nutrition()["fatContent"])
        recipe_hash["carbohydrateContent"]  = recipe.convert_nutrition_to_grams(recipe.get_recipe_nutrition()["carbohydrateContent"])
        recipe_hash["proteinContent"]       = recipe.convert_nutrition_to_grams(recipe.get_recipe_nutrition()["proteinContent"])
        recipe_hash["difficulty"]           = recipe.get_recipe_difficulty()
        recipe_hash["servings_description"] = recipe.get_recipe_servings_description()
        recipe_hash["servings"]             = recipe.get_recipe_servings()
        # optional metadata
        recipe_hash["categories"]           = recipe.get_recipe_categories()
        recipe_hash["description"]          = recipe.get_recipe_description()
        recipe_hash["keywords"]             = recipe.get_recipe_keywords()
        recipe_hash["reviews"]              = recipe.get_recipe_reviews()
        recipe_hash["author"]               = recipe.get_recipe_author()
        recipe_hash["rating"]               = recipe.get_recipe_rating()
        recipe_hash["review_count"]         = recipe.get_recipe_review_count()
        recipe_hash["recipe_image_url"]     = recipe.get_recipe_image()
        # get on dict will return None if does not exist instead of throw error
        recipe_hash["saturatedFatContent"]  = recipe.convert_nutrition_to_grams(recipe.get_recipe_nutrition().get("saturatedFatContent",None))
        recipe_hash["cholesterolContent"]   = recipe.convert_nutrition_to_grams(recipe.get_recipe_nutrition().get("cholesterolContent",None))
        recipe_hash["sugarContent"]         = recipe.convert_nutrition_to_grams(recipe.get_recipe_nutrition().get("sugarContent",None))
        recipe_hash["fiberContent"]         = recipe.convert_nutrition_to_grams(recipe.get_recipe_nutrition().get("fiberContent",None))
        recipe_hash["sodiumContent"]        = recipe.convert_nutrition_to_grams(recipe.get_recipe_nutrition().get("sodiumContent",None))

    except Exception as e:
        recipe_hash = {"Status":e}
    return recipe_hash

if __name__=="__main__":
    recipe_url = "https://www.foodnetwork.com/recipes/ellie-krieger/roasted-salmon-with-shallot-grapefruit-sauce-recipe-1947996"
    #my_recipe = validate_recipe(recipe_url)
    #print(my_recipe)



