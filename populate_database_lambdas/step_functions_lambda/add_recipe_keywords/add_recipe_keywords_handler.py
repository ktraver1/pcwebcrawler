"""
This file is lambda handler that adds rows to recipes keywords table
@author Kevin Travers
"""
import boto3
import logging
import pymysql
logger = logging.getLogger()
logger.setLevel(logging.INFO)
def lambda_handler(event, context):
    """
    Lambda function that will create keyword id
    @param url String
    @param recipe id Int
    """
    add_keywords(event)
    # don't need to return the keyword ids if deleteing will delete the based off recipe_id 
    # and keep keywords since other recipes might be using them
    return event

def add_keywords(recipe_hash):
    """
    This method creates keyword rows in database
    @param recipe_name String name of the recipe
    """
    try:
        #rds settings get values stored encrypted in aws parameter store
        ssm = boto3.client('ssm')
        #passowrd to connect to database
        db_password = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Password', WithDecryption=True)['Parameter']['Value']
        #username to connect to database
        db_username = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Username', WithDecryption=True)['Parameter']['Value']
        # aws endpoint is the location of the database
        db_endpoint = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Endpoint', WithDecryption=True)['Parameter']['Value']
        # name of the database
        db_name     = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Name', WithDecryption=True)['Parameter']['Value']
        # extract recipe data needed for function
        recipe_keywords = recipe_hash["keywords"]
        recipe_id       = recipe_hash["recipe_id"]
        # add each keyword by looping through keyword array and store ids created
        keyword_ids = []
        # establish connection to rds database
        try:
            conn = pymysql.connect(db_endpoint, user=db_username, passwd=db_password, db=db_name, connect_timeout=5)
        except Exception as e:
            logger.error("ERROR: {}".format(e))
            raise e
        logger.info("SUCCESS: Connection to RDS MySQL instance succeeded")
        #create keywords in table
        with conn.cursor() as cur:
            # check what keywords already exists and only add new ones but re-use keyword ids
            # don't delte keywords since recipes will share keywords
            # since most of keywords will be same will select searhc if exist and if does not insert
            # this works faster then other appracohes only if allot of duplications which there will be
            sql_serach = "select keywordID from keywords where keyword = %s"
            sql_insert = "INSERT INTO keywords(keyword)  SELECT %s  FROM dual WHERE NOT EXISTS (SELECT * FROM keywords WHERE keyword=%s)"
            #sql = 'insert into keywords (keyword) values(%s)'
            for keyword in recipe_keywords:
                # need recipe id to assocate row with this recipe
                values_insert  = (keyword,keyword)
                values_serach = (keyword)
                cur.execute(sql_serach, values_serach)
                #check if keyword already exists
                keywordID = None
                result_tuple = cur.fetchone()
                # slect returns tuple with one value 
                # if that value is None then need insert the keyword
                # else keyword already exists and it returned its keyword ID 
                if result_tuple:
                  keywordID = result_tuple[0]
                else:
                  #does not exist and need to insert into table
                  cur.execute(sql_insert, values_insert)
                  keywordID = cur.lastrowid
                  if not keywordID:
                    # if during time between select and insert another lambda inserted keyword then this will return 0
                    # so need to get keyword via select
                    # should be rare but can happen 
                    cur.execute(sql_serach, values_serach)
                    keywordID = cur.fetchone()
                conn.commit()
                # append keyword ids
                keyword_ids.append(keywordID)
            # sql = "insert into recipeKeyWords (recipeID,keywordID) values(%s,%s)"
            # insert into recipeKeyWords if does not exists already
            sql = "INSERT INTO recipeKeyWords(recipeID,keywordID) SELECT %s,%s  FROM dual WHERE NOT EXISTS (SELECT * FROM recipeKeyWords WHERE recipeID=%s and keywordID=%s);"
            for keyword_id in keyword_ids:
                # need recipe id to assocate row with this recipe
                values  = (recipe_id,keyword_id,recipe_id,keyword_id)
                cur.execute(sql, values)
                conn.commit()
    except Exception as e:
        logger.error("ERROR: {}".format(e))
        raise e   

    