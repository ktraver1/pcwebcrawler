"""
This file handles all tasks dealing with the URL
@author Kevin Travers
"""
from urllib.parse import urlparse
from urllib.parse import urljoin

def get_site_name(url):
    """
    This method returns the site name of the given url string
    @param url (name.example.com)
    @return string (example)
    """
    try:
        results = get_sub_domain_name(url).split('.')
        return results[-2]
    except Exception as e:
        raise e
    

def get_sub_domain_name(url):
    """
    This method returns the sub-domain of the given url string
    @param string url (name.example.com/path/to/page)
    @return string (example.com)
    """
    try:
        return  urlparse(url).netloc
    except Exception as e:
        raise e


def get_domain_name(url):
    """
    This method gets the domain for URL 
    @param string url (name.example.com/path/to/page)
    @return string (name.example.com)
    """
    try:
        results = get_sub_domain_name(url).split('.')
        return results[-2] + '.' + results[-1]
    except Exception as e:
        raise e
        

def join_URL(base,path):
    """
    This method returns full url for the path
    Sometimes links are not full urls (path/to/file) and need to join with domain base
    @param string base (path/to/page)
    @param string path (name.example.com)
    @return String (name.example.com/path/to/page)
    """
    try:
        return urljoin(base, path)
    except TypeError as e:
        raise e
    except Exception as e:
        raise e

