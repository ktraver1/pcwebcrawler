import unittest
from link_finder import *

class TestLinkeFinder(unittest.TestCase):
    """
    Unit tests for link_finder.py
    """
    def test_find_links(self):
        #test find_links is working as intended with correct input
        target_homepage = 'https://pythonprogramming.net/'
        url = "https://pythonprogramming.net/robotics-tutorials/"
        result = find_links(target_homepage,url)
        links = {'https://pythonprogramming.net/web-development-tutorials/',
                'https://pythonprogramming.net/gui-development-tutorials/',
                'https://pythonprogramming.net/consulting/',
                'https://pythonprogramming.net/data-analysis-tutorials/',
                'https://pythonprogramming.net/support/',
                'https://pythonprogramming.net/go/',
                'https://pythonprogramming.net/robotics-tutorials/#golang',
                'https://pythonprogramming.net/login/',
                'https://pythonprogramming.net/+=1/',
                'https://pythonprogramming.net/bot-tutorials/',
                'https://pythonprogramming.net/support-donate/',
                'https://pythonprogramming.net/about/tos/',
                'https://pythonprogramming.net/about/privacy-policy/',
                'https://pythonprogramming.net/',
                'https://pythonprogramming.net/robotics-tutorials/',
                'https://pythonprogramming.net/python-fundamental-tutorials/',
                'https://pythonprogramming.net/game-development-tutorials/',
                'https://pythonprogramming.net/register/'}
        self.assertEqual(result, links)
    def test_is_valid_link(self):
        #test is_valid_link is working as intended with correct input
        url = 'https://pythonprogramming.net/register/'
        homepage = 'https://pythonprogramming.net/'
        self.assertTrue(is_valid_link(url,homepage))
    def test_not_is_valid_link(self):
        #test is_valid_link is working as intended with correct input
        url = 'https://google.com/register/'
        homepage = 'https://pythonprogramming.net/'
        self.assertFalse(is_valid_link(url,homepage))

if __name__ == "__main__":
    unittest.main()