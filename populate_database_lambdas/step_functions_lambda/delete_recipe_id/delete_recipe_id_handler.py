"""
This file is lambda handler that removes row from recipes table 
@author Kevin Travers
"""
import boto3
import logging
import pymysql
logger = logging.getLogger()
logger.setLevel(logging.INFO)
def lambda_handler(event, context):
    recipe_id = event["recipe_id"]
    result = delete_recipe_row(recipe_id)
    #TODO Handle error case better
    return event

def delete_recipe_row(recipe_id):
    """
    This method deletes recipe from row in database
    @param recipe_name Hash name of the recipe
    """
    try:
        #rds settings get values stored encrypted in aws parameter store
        ssm = boto3.client('ssm')
        #passowrd to connect to database
        db_password = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Password', WithDecryption=True)['Parameter']['Value']
        #username to connect to database
        db_username = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Username', WithDecryption=True)['Parameter']['Value']
        # aws endpoint is the location of the database
        db_endpoint = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Endpoint', WithDecryption=True)['Parameter']['Value']
        # name of the database
        db_name     = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Name', WithDecryption=True)['Parameter']['Value']
        # establish connection to rds database
        try:
            conn = pymysql.connect(db_endpoint, user=db_username, passwd=db_password, db=db_name, connect_timeout=5)
        except Exception as e:
            logger.error("ERROR: {}".format(e))
            raise e
        result = None
        # attempt to delte row with same recipe id as input
        with conn.cursor() as cur:
            sql     = "delete from recipes where recipeID=%s"
            values  = (recipe_id)
            cur.execute(sql, values)
            conn.commit()
            #if cur.rowcount > 0:
            #    result = {"Success":"Row was deleted"}
            #else:
            #    raise "Did not delete any rows, recipe id might not exist"
        return result  
    except Exception as e:
        logger.debug("ERROR: {}".format(e))
        raise e
if __name__ == "__main__":
    result = delete_recipe_row({"recipe_id":3})
    print(result)