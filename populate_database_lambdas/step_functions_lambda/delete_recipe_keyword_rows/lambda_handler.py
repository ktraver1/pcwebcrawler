"""
This file is lambda handler that deletes rows from recipes keywords  assocation table
Does not delete actualy keyword since other recipes share keywords but deletes assocations
@author Kevin Travers
"""
import boto3
import pymysql
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event,context):
    """
    Will use recipe_id to delete keywords assocaited with that id
    @param event is the recipe_hash(infomation about the recipe collected from source)
    """
    recipe_id = event["recipe_id"]
    delete_rds_row(recipe_id)
    return event

def delete_rds_row(recipe_id,region="us-east-1"):
    """
    This method deletes keywords asscocations row in database
    @param recipe_id int recipe_id of the recipe
    @param region string region database is in
    """
    try:
        # rds settings get values stored encrypted in aws parameter store
        ssm = boto3.client('ssm')
        # passowrd to connect to database
        db_password = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Password', WithDecryption=True)['Parameter']['Value']
        # username to connect to database
        db_username = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Username', WithDecryption=True)['Parameter']['Value']
        # aws endpoint is the location of the database
        db_endpoint = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Endpoint', WithDecryption=True)['Parameter']['Value']
        # name of the database
        db_name     = ssm.get_parameter(Name='/PersonalChef/Prod/DB/Name', WithDecryption=True)['Parameter']['Value']
        # establish connection to rds database
        try:
            conn = pymysql.connect(db_endpoint, user=db_username, passwd=db_password, db=db_name, connect_timeout=5)
        except Exception as e:
            logger.error("ERROR connecting to RDS: {}".format(e))
            raise e
        logger.info("SUCCESS: Connection to RDS MySQL instance succeeded")
        # remove recipe keywords assocation
        with conn.cursor() as cur:
            # set up varibales for sql query
            sql_query   = "delete from recipeKeyWords where recipeID = %s"
            values      = (recipe_id)
            # execute query 
            cur.execute(sql_query, values)
            # commit query so row is deleted
            conn.commit()
    except Exception as e:
        logger.error("ERROR: {}".format(e))
        raise e

