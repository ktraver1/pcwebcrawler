from urllib.request import urlopen
from link_finder import *
from domain import *
from cloud_library import *


class Spider:
    #class variables
    project_name = ''
    base_url = ''
    domain_name = ''
    queue_name = ''
    table_name = ''


    def __init__(self, project_name, base_url, domain_name):
        """
        initate Spider threaded classes
        @params String project_name name used for this projectt
        @params String base_url target website
        @params String domain_name domain of the target website 
        """
        Spider.project_name = project_name
        Spider.base_url = base_url
        Spider.domain_name = domain_name
        Spider.queue_name = Spider.project_name + 'Queue'
        Spider.table_name = Spider.project_name + 'Table'

    # Updates and fills queue 
    @staticmethod
    def crawl_page(page_url):
        """
        method crawls page anc collects all links
        @param String page_url page url that needs to be crawled
        """
        try:
            aws = AwsAPI()
            # in crawled stored in table so i dont crawl page i have crawled already, and not to put item in queue multiple times
            response = aws.get_item_dynamodb_table(Spider.table_name,page_url)
            if not response:
                #item is not in dynomtable, skip this item
                return 
            # crawled = 1 its been crawled, 0 not been crawled
            #check if page url has already been crawled
            if not response["crawled"]:
                #crawl page and all found links added to queue
                Spider.add_links_to_queue(Spider.gather_links(page_url))
                #update table for url to crawled, 1 means crawled
                aws.update_item_dynamodb_table(Spider.table_name,page_url,1)
        except Exception as e:
            raise e
    # Converts raw response data into readable information and checks for proper html formatting
    @staticmethod
    def gather_links(page_url):
        """
        method crawls page and collects all links
        @param String page_url page url that needs to be crawled
        @return set of links found on url page
        """
        result = None
        try:
            result = find_links(page_url,Spider.base_url)
        except Exception as e:
            #logging
            return None
        return result

    # Saves queue data to project files
    @staticmethod
    def add_links_to_queue(links):
        """
        method add links to dynoamdb table that is not already queued,crawled
        @param set links to be added to dynomadb
        """
        try:
            #if didn't find any links then nothing to add
            if links:
                #loop through links and if not already seen before add to table and sqs
                for url in links:
                    #if in table then it has been seen before
                    aws = AwsAPI()
                    response = aws.is_in_dynamodb_table(Spider.table_name,url)
                    if not response:           
                        #add to table as not crawled and push to sqs
                        aws.add_item_to_dynamodb_table(Spider.table_name,url)
                        aws.push_to_queue(Spider.queue_name,url)
        except Exception as e:
            raise e